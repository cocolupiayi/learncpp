#include "openmp.hpp"
#include <iostream>
#include <omp.h>

Openmp::Openmp()
{
  omp_set_num_threads(8);
}

Openmp::~Openmp()
{
}

void Openmp::printHelloWorld(int count)
{
#pragma omp parallel for num_threads(8)
  for (int i = 0; i < 10000; i++)
  {
    int tid = omp_get_thread_num();
    std::cout << "Hello World : " << tid << "\n";
  }
}