#pragma once
#include <thread>
#include <mutex>
#include <future>

class Thread
{
private:
  std::mutex *mutex;
  std::promise<int> promise;
  std::future<int> future;

public:
  Thread();
  ~Thread();
  void thread1_function();
  void thread2_function();
  void thread_future();
};
