#include <iostream>
#include "thread.hpp"

Thread::Thread()
{
    mutex = new std::mutex();
    future = promise.get_future();
}

Thread::~Thread()
{
    delete[] mutex;
};

/**
 * Function Pointer
 *
 */
void Thread::thread1_function()
{
    for (int i = 0; i < 1000; i++)
    {
        if (i == 999)
            promise.set_value(i);
        // mutex->lock();
        std::cout << "thread1: " << i << "\n";
        // mutex->unlock();
    }
}

void Thread::thread2_function()
{
    for (int i = 0; i < 1000; i++)
    {
        // future.get();
        mutex->lock();
        std::cout << "thread2: " << i << "\n";
        mutex->unlock();
    }
}

void Thread::thread_future()
{
    {
        int temp = future.get();
        // mutex->lock();
        std::cout << "thread2: " << temp << "\n";
        // mutex->unlock();
    }
}
