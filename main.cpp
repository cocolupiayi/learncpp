#include <iostream>
#include "src/thread/thread.hpp"
#include "src/openmp/openmp.hpp"

int main()
{
  /*
    Thread Class
  */

  // Thread thread;
  // std::thread thread1(&Thread::thread1_function, &thread);
  // std::thread thread2(&Thread::thread_future, &thread);
  // thread1.join();
  // thread2.join();

  /*
    OpenMP Class
  */

  Openmp openmp;
  openmp.printHelloWorld(10000);

  // #pragma omp parallel for num_threads(8)
  //   for (int i = 0; i < 10000; i++)
  //   {
  //     int tid = omp_get_thread_num();
  //     std::cout << "Hello World : " << tid << "\n";
  //   }

  return 0;
}
